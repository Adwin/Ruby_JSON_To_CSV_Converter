require 'csv'
require 'json'

# This methods recursively transform empty values to empty strings
def emptyToString(value)
  value.each_with_object({}) do |(k,v), item|
    case v
    when Hash
      item[k] = emptyToString v
    when nil
      item[k] = ''
    else
      item[k] = v
    end
  end
end

def getColumnName(object, path='')
  scalars = [String, Integer, FalseClass, TrueClass]
  columns = {}

  if [Hash, Array].include? object.class
    object.each do |k, v|
      newColumns = getColumnName(v, "#{path}#{k}.") if object.class == Hash
      newColumns = getColumnName(k, "#{path}#{k}.") if object.class == Array
      columns = columns.merge newColumns
    end

    return columns
  elsif scalars.include? object.class
      # We get ride of the trailing slash
      lastPart = path[0, path.length - 1]
      columns[lastPart] = object
      return columns
  else
    return {}
  end
end

# We define the pattern of the row
def getRowPattern(json)
  treatedElements, remainingElements = [], json
  while !remainingElements.nil?

    return remainingElements if remainingElements.is_a? Array

    if remainingElements.is_a? Hash
      remainingElements.each do |k, v|
        treatedElements.push remainingElements[k]
      end
    end

    remainingElements = treatedElements.shift
  end

  return [json]
end

#We try to load the json file, and emit an exception in case of an error
def loadFile(path)
  # Load input file
  begin
    raw = IO.read(path)
  rescue Exception => e
    emitExceptionLogError(e, true)
  end

  # Try to parse json from loaded data
  begin
    return JSON.load(raw)
  rescue Exception => e
    emitExceptionLogError(e, false)
  end
  nil
end

def emitExceptionLogError(e, fileNotFound)
  puts 'Invalid json, see error.txt' if fileNotFound === false
  puts 'File not located, see error.txt' if fileNotFound === true
  File.open('error.txt', 'w+') { |f| f.write(e.to_s) }
end


print "What file do we convert ?"

path = gets.chomp

print "What is the exit file's name ? "

exitPath = gets.chomp

csvOutputOptions = {
  col_sep: ','
}

#json = JSON.parse(File.open(path).read)
json = loadFile(path)
jsonElements = getRowPattern json
jsonElements.map! { |x| emptyToString x }

csvResultElements = []
jsonElements.each do |row|
  csvResultElements[csvResultElements.length] = getColumnName row
end

columnNamesWritten = false
CSV.open(exitPath + '.csv', 'w+', csvOutputOptions) do |csv|
  csvResultElements.each do |row|
    #We start by writing columns names, we pass here only once
    csv << row.keys && columnNamesWritten = true if columnNamesWritten === false
    #We then pass here for every element.
    csv << row.values
  end
end

print "JSON succesfully converted"