To use the json to csv converter, please type 

ruby jsonToCsvConverter.rb 

in your shell.

You will then be invited to choose the json file you want to convert, and also a name for the result file.

If the selected file is not a Json or is not a valid json file, the conversion will fail, and you will a short report error in the error.txt file.